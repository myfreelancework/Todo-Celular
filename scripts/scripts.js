/**
 * jQuery Unveil
  */

/**
 * jQuery Unveil
 * A very lightweight jQuery plugin to lazy load images
 * http://luis-almeida.github.com/unveil
 *
 * Licensed under the MIT license.
 * Copyright 2013 Luís Almeida
 * https://github.com/luis-almeida
 */

;(function($) {

  $.fn.unveil = function(threshold, callback) {

    var $w = $(window),
        th = threshold || 0,
        retina = window.devicePixelRatio > 1,
        attrib = retina? "data-src-retina" : "data-src",
        images = this,
        loaded;

    this.one("unveil", function() {
      var source = this.getAttribute(attrib);
      source = source || this.getAttribute("data-src");
      if (source) {
        this.setAttribute("src", source);
        // console.log(this);
        if (typeof callback === "function") callback.call(this);
      }
    });

    function unveil() {

      var offset_propio=200;

      var inview = images.filter(function() {
        var $e = $(this);
        if ($e.is(":hidden")) return;

        var wt = $w.scrollTop()+offset_propio,
            wb = wt + $w.height(),
            et = $e.offset().top,
            eb = et + $e.height();

        return eb >= wt - th && et <= wb + th;
      });

      loaded = inview.trigger("unveil");
      images = images.not(loaded);
    }

    $w.scroll(unveil);
    $w.resize(unveil);

    unveil();

    return this;

  };

})(window.jQuery || window.Zepto);



$(function(){
	
	 $(".two_column img,.threcolums img,.videos_phones_container img,#topics_shadow img,#topics_blue img,#topics img,#versus img").unveil();


	margin_right=7;
	// extraspace=50;
	
	//****** Calculate widh column phones
	numcols=$("#phone_columns .phone_column").length;

	if(numcols>3){
		widthcol=$("#phone_columns .phone_column").eq(0).width()+margin_right;
		withcontainer=(widthcol*numcols)-margin_right;
		$("#phone_columns").width(withcontainer);
		$("#compare_container2").css("padding-right","52px");
		// $("body").css("overflow-x","visible");
	}

	//****** Calculate widh video_thumbs

	numb_thumbs=$("#thumbs_videos ul li").length;
	num_cols_thumbs=Math.ceil(numb_thumbs/3);
	$("#thumbs_videos").width(num_cols_thumbs*220);
	if($('#phone_columns').length){
		$('#phone_columns').sortable({
									// { items: "> .phone_column",
									  handle: ".big_phone img",
									  axis:"x", 
									  opacity: 0.7,
									  tolerance: "pointer"
									 });
	}
	// $("#phone_columns").shapeshift();
	//****** sHOW / HIDE Fixed Bar

	var limite=670;
	var b=0;
	$(window).on('scroll', function(){
		pos=$(window).scrollTop();
		if(pos>limite && b==0 && pos<2700){ 
			$("#floatbar").fadeIn("fast");
			b=1;
		}else if( ( pos<limite && b==1 ) || (pos>2700 && b==1)  ){
			$("#floatbar").fadeOut("fast");			
			b=0;
		}
	});


	/*	$('#phone_columns').dragswap({ 
			dropAnimation: true,
			element:'div.phone_column',
			overClass: 'over_column',
			moveClass: 'moving_column', 
			dropClass: 'drop_column',
			dropComplete:function(){
				console.log("termino la animacion");
			}
	});*/


    // SELECT FIRST THUMB SLIDER
    big_container= $(".big_container");
    change_slide($("#main_slider ul li:eq(0) a"));
    $("#main_slider ul a").on("mouseenter",change_slide);

});

function change_slide(e){

        enlace=(e.target)?$(this):e;

        big_container.find('.big_image').attr('src',enlace.attr('data-big-image'));

        enlace.parents('ul').find('li').removeClass();
        enlace.parent().addClass('active');

        paragraph= big_container.find('p');
        paragraph.html(enlace.find('p').html());
        paragraph.parent('a').attr('href',enlace.attr('href'));

        apple_tile= big_container.find('.apple_tile'); // for apple blog
        apple_tile.children('em').html(enlace.attr('data-number-comment'));
        apple_tile.parent('a').attr('href',enlace.attr('href')+"#comments");
        
        comment_desc=big_container.find('#comment_desc strong'); // for android blog
        comment_desc.html(enlace.attr('data-number-comment'));
        comment_desc.parent('a').attr('href',enlace.attr('href')+"#comments");
}


function switchtab(num){
  var th=document.getElementById("th"+num);
  var ths=document.querySelectorAll('.tabs ul a');
   for(var i=0;i<ths.length;i++){
     ths[i].className='';
   }
   th.className="activo";

  var tcs=document.querySelectorAll('.tabs_content');
  for(var i=0;i<tcs.length;i++){
    tcide=(tcs[i].getAttribute('id').charAt(tcs[i].getAttribute('id').length-1));
    if(num!=tcide)
     tcs[i].style.display='none';
    else
     tcs[i].style.display='block';
  }

  return false;
}